<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class InterviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
                'date' => '2019-5-1',
                'summary' => 'interview went well'
            ],
            [
                'date' => '2020-1-1',
                'summary' => 'interview went bad'
            ],            [
                'date' => Carbon::now(),
                'summary' => 'interview was late'
            ],                  
            ]);         
    }
}
