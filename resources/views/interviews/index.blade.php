@extends('layouts.app')

@section('title', 'interviews')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
@can('admin')
<div><a href =  "{{url('/interviews/create')}}"> Add new interview</a></div>
@endcan
@can('manager')
<div><a href =  "{{url('/interviews/create')}}"> Add new interview</a></div>
@endcan


<h1>interviews list</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>candidate</th><th>interviewer</th><th>date</th><th>summary</th>
    </tr>

    @if(isset($isempty) && $isempty)
        you dont have any interviews
    @endif
    <!-- the table data -->
    @foreach($interviews as $interview)
        <tr>
            <td>{{$interview->id}}</td>
            <td>
            @if(isset($interview->candidate))

                {{$interview->candidate->name}}
            @else
                No Candidate Assigned
            @endif
            
            </td>
            <td>
            @if(isset($interview->user))

                {{$interview->user->name}}
            @else
                No interviewer Assigned
            @endif
            
            </td>
            <td>{{$interview->date}}</td>
            <td>{{$interview->summary}}</td>                                                          
        </tr>
    @endforeach
</table>
@endsection

