@extends('layouts.app')

@section('title', 'Create candidate')

@section('content')
        <h1>Create interview</h1>
        <form method = "post" action = "{{action('InterviewsController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "date">interview date</label>
            <input type = "date" class="form-control" name = "date">
        </div>     
        <div class="form-group">
            <label for = "summary">interview summary</label>
            <input type = "text" class="form-control" name = "summary">
        </div> 
        <div class="form-group">
            <label for="candidate_id">Choose a candidate:</label>
            <select name="candidate_id" id="candidate_id">

                @foreach($candidates as $candidate)

                    <option value="{{$candidate->id}}">{{$candidate->name}}</option>

                @endforeach

            </select>
        </div>
        <div class="form-group">
            <label for="user_id">Choose an interviewer:</label>
            <select name="user_id" id="user_id">

                @foreach($users as $user)

                    <option value="{{$user->id}}">{{$user->name}}</option>

                @endforeach

            </select>
        </div>


        <div>
            <input type = "submit" name = "submit" value = "Create interview">
        </div>                       
        </form>    
@endsection
